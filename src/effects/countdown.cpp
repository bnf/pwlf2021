//
// Created by James Moore on 1/26/21.
//

#include "countdown.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 60;
const int digit_width = 3;

void countdown(int duration) {
    FastLED.clear();
    resetStopwatch();

//    CHSV e_color = { 0, 255, 255};
//    CHSV s_color = { 192, 255, 255};

    // countdown
//    for (int countdown_n = 3; countdown_n > 0; countdown_n--) {
//
//        for (int east_pos = EAST_START; east_pos >= 0; east_pos--) {
//
//            FastLED.clear();
//
//            for (int digits = 0; digits < countdown_n * (digit_width + 1); digits += 4) {
//                for (int pix = 0; pix < digit_width; pix++) {
//                    int draw_pos = east_pos - digits - pix;
//                    e_color.hue = map(draw_pos, EAST_START, 0, 0, 255);
//                    if (draw_pos >= 0) leds[draw_pos] = e_color;
//
//                    draw_pos = CENTER + (CENTER - east_pos) + digits + pix;
//                    s_color.hue = map(draw_pos, SOUTH_START, NUM_LEDS, 0, 255);
//                    if (draw_pos <= NUM_LEDS) leds[draw_pos] = s_color;
//                }
//            }
//            FastLED.show();
//            FastLED.delay(1000 / FRAMES_PER_SECOND);
//
//        }
//    }

    // lights up
    for (int i = 0; i <= NUM_LEDS; i++) {
        leds[i] = CRGB::White;
    }
    FastLED.show();
    FastLED.delay(2000);

    // rainbow cycle
    for (int i = 0; i <= NUM_LEDS; i++) {
        int hue = map(i, 0, NUM_LEDS, 0, 255);
        leds[i] = CHSV(hue, 255, 255);
        FastLED.show();
        FastLED.delay(1000 / 100);
    }

    int hue_offset = 0;
    resetStopwatch();
    while (stopwatch < duration * 1000) {
        updateTimers();
        for (int i = 0; i <= NUM_LEDS; i++) {
            int hue = map(i + hue_offset, 0, NUM_LEDS, 0, 255);

            leds[i] = CHSV(hue, 255, 255);
        }
        hue_offset--;
        FastLED.delay(1000 / 100);
        FastLED.show();
    }

    for (int f = 0; f < 12; f++) {
        for (int i = 0; i <= NUM_LEDS; i++) {
            leds[i].fadeToBlackBy(40);
        }
        FastLED.show();
        FastLED.delay(1000 / 50);
    }

}