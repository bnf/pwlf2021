//
// Created by James Moore on 2/5/21.
//

#include <Arduino.h>
#include <lib/shared.h>
#include "fireworks.h"

typedef struct fw {
    CHSV color;
    int delay;
    int center;
    int ttl;
    int age;
    int particles;
    bool started;
} fw;

const int num_fw = 5;
fw all_fws[num_fw];

void print_fw(fw* f) {
    Serial.printf("Hue: %i\tDelay: %i\tAge: %i\tTTL: %i\tC Pos: %i\n",
                  f->color.hue, f->delay, f->age, f->ttl, f->center);
}

void reset_fw(fw* f) {
    f->color = CHSV(random8(), 255, 255);
    f->delay = random(300, 1000);
    f->center = random(10, NUM_LEDS - 10);
//    f->center = 20;
    f->started = false;
    f->age = 0;

    f->particles = random8(2, 5);
    f->ttl = f->particles * 400;

//    print_fw(f);
}

void run_fw(fw* f) {

    // run down the delay timer
    if (f->delay > 0) {
        f->delay -= dt;
        return;
    }

    // reap dead fireworks
    if (f->age > f->ttl) {
        reset_fw(f);
        return;
    }

    if (! f->started) {
        // draw initial center
        leds[f->center - 1] = f->color;
        leds[f->center] = f->color;
        leds[f->center + 1] = f->color;
        f->started = true;
    } else {
        // draw explosion
        float p = map((float)f->age, 0, f->ttl, 0.0, 1.0);
        float eased_p = QuarticEaseOut(p);
        int offset = lround(map(eased_p, 0.0, 1.0, 0, 15));

        if (eased_p < 0.98) {
            for (int i = 1; i <= f->particles; i++) {
                int adj_offset = offset / i;
//                Serial.printf("%i\t%i\t%i\n", f->age, f->ttl, adj_offset);

                if (f->center + adj_offset < NUM_LEDS) leds[f->center + adj_offset] = f->color;
                if (f->center - adj_offset >= 0) leds[f->center - adj_offset] = f->color;

            }
        }
    }

    f->age += dt;

}

void fireworks(int duration) {

    updateTimers();
    resetStopwatch();

    for (int i = 0; i < num_fw; i++) {
        reset_fw(&all_fws[i]);
    }

    while(stopwatch < duration * 1000) {
        updateTimers();

        fadeToBlackBy(leds, NUM_LEDS, 10);

        for (int i = 0; i < num_fw; i++) {
            run_fw(&all_fws[i]);
        }

        FastLED.show();
    }

    fadeOut(0, NUM_LEDS);
}