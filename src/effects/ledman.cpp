//
// Created by James Moore on 2/3/21.
//

#include "ledman.h"
#include "lib/shared.h"

typedef struct pill {
    int position;
} pill;

const int num_pills = 60;
pill pills[num_pills];

typedef struct character {
    CHSV color;
    float p{};
    float v{};
    bool dead{};
    bool edible{};
} character;

const character CHARACTER_DEFAULT = {
        .color = CHSV(255, 255, 255),
        .p = 0,
        .v = -0.05,
        .dead = false,
        .edible = false
};

const int powerpill_offset = 4;
const int pp_pos = powerpill_offset;
bool paused;
bool pill_eaten;
bool draw_score;

character blinky;
character pinky;
character inky;
character clyde;
character lman;

void draw_powerpill() {
    int brightness = beatsin8(240, 0, 255);
    leds[pp_pos] = CHSV(0, 0, brightness);
//    leds[pp_pos + 1] = CHSV(0, 0, brightness);
}

bool in_bounds(character c) {
    int i_pos = round(c.p);

    return (i_pos >= 0 && i_pos <= NUM_LEDS);
}

void draw_ledman() {

    if (draw_score) return;

    int i_pos = round(lman.p);

    if (in_bounds(lman)) {
        leds[i_pos] = lman.color;
    }
    if (pill_eaten && in_bounds(lman)) {
        CHSV adj_color = lman.color;
        adj_color.value = beatsin8(240, 0, 255);

        if (lman.v > 0) {
            leds[i_pos + 1] = adj_color;
        } else {
            leds[i_pos - 1] = adj_color;
        }
    }

    if (lman.p < 0 && !blinky.dead && !inky.dead && !pinky.dead && !clyde.dead) {
        // make sure ledman wraps around the first time
        lman.p = NUM_LEDS;
    }
}

void draw_ghost(character* ghost) {
    int i_pos = round(ghost->p);

    if (in_bounds(*ghost)) {

        if (pill_eaten && !ghost->edible) {
            // turn ghosts blue
            ghost->edible = true;
            ghost->v *= -1.0;
            ghost->color.hue = HUE_BLUE;
//        } else if (round(ghost->p) == CENTER - 15 && ghost->edible && !ghost->dead) {
//             reverse blue ghost when they get near the corner
//            ghost->v *= -1.0;
        }

        if (round(ghost->p) == round(lman.p) && !ghost->dead) {
            // ghost is eaten
            ghost->dead = true;
            ghost->color = CHSV(0, 0, 255);
            ghost->v *= 1.8;
            paused = true;
            draw_score = true;
            leds[round(lman.p)] = CRGB::Cyan;

        } else {
            leds[i_pos] = ghost->color;
        }

    }

}

void draw_pills() {
    for (int i = 0; i < num_pills; i++) {

        if (round(lman.p) == pills[i].position) {
            pills[i].position = -1;
        }

        if (pills[i].position > 0) {
            leds[pills[i].position] = CHSV(0, 0, 100);
        }
    }

}

void update_positions() {
    lman.p += lman.v;
    blinky.p += blinky.v;
    inky.p += inky.v;
    pinky.p += pinky.v;
    clyde.p += clyde.v;
}

void ledman() {

    // reset positions on subsequent runs
    lman = CHARACTER_DEFAULT;
    lman.color.hue = HUE_YELLOW;
    lman.p = EAST_START ;

    blinky = CHARACTER_DEFAULT;
    blinky.color.hue = HUE_RED;
    blinky.p = lman.p + 6;

    pinky = CHARACTER_DEFAULT;
    pinky.color.hue = HUE_PINK;
    pinky.p = lman.p + 8;

    inky = CHARACTER_DEFAULT;
    inky.color.hue = HUE_AQUA;
    inky.p = lman.p + 10;

    clyde = CHARACTER_DEFAULT;
    clyde.color.hue = HUE_ORANGE;
    clyde.p = lman.p + 12;

    for (int i = 0; i < num_pills; i++) {
        pills[i].position = 2 * i;
    }

    paused = true;
    pill_eaten = false;
    draw_score = false;
    FastLED.clear();

    // draw dots
    draw_pills();

    updateTimers();
    resetStopwatch();
    while (stopwatch < 1000) {
        updateTimers();
        draw_powerpill();
        draw_ledman();
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        FastLED.show();
    }

    paused = false;

    // ledman and ghosts move towards pill
    while (lman.p >= pp_pos) {
        FastLED.clear();
        draw_pills();
        draw_powerpill();
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        draw_ledman();
        FastLED.show();
        update_positions();
    }

    // ledman eats power pill, ghosts turn blue
    pill_eaten = true;
//    lman.v *= 1.25;

    while (lman.p >= 0 ||
           blinky.p <= NUM_LEDS ||
           inky.p <= NUM_LEDS ||
           pinky.p <= NUM_LEDS ||
           clyde.p <= NUM_LEDS) {


        FastLED.clear();
        draw_pills();
        // draw ghosts in reverse order so eyes appear
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        draw_ledman();
        FastLED.show();
        update_positions();

        if (paused) {
            Serial.printf("%f %f %f %f %f\n",
                          lman.p,
                          blinky.p,
                          pinky.p,
                          inky.p,
                          clyde.p);
            paused = false;
            draw_score = false;
            FastLED.delay(400);
        }
    }

    fadeOut(0, NUM_LEDS);

}