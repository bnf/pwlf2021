//
// Created by James Moore on 1/24/21.
//

#include "rgb.h"
#include "lib/shared.h"

const int run_time = 10000;

void rgb() {
    resetStopwatch();

    while (stopwatch < run_time) {
        updateTimers();

        for (int pix = 0; pix < 40; pix++) {
            leds[pix] = CRGB::Red;
        }
        for (int pix = 40; pix < 80; pix++) {
            leds[pix] = CRGB::Green;
        }
        for (int pix = 80; pix < 120; pix++) {
            leds[pix] = CRGB::Blue;
        }

        FastLED.show();
    }

    fadeOut(0, NUM_LEDS);

}