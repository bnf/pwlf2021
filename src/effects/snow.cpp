//
// Created by James Moore on 1/26/21.
//

#include "snow.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 60;
const int fadeAmount = 10;
const int fadedBrightness = 100;
const int flakeSat = 255;

int flakeTime = 0;

void snow(int duration) {

    FastLED.clear();

    resetStopwatch();

    while (stopwatch < duration * 1000) {
        updateTimers();
        flakeTime += dt;

        for (int i = 0; i <= NUM_LEDS; i++) {
            leds[i].fadeToBlackBy(fadeAmount);
        }

        if (flakeTime > 50) {
            flakeTime = 0;
            int new_pos = random8(1, NUM_LEDS - 1);
            int flakeColor = random8(255);

            leds[new_pos - 1].setHSV(flakeColor, flakeSat, fadedBrightness);
            leds[new_pos].setHSV(flakeColor, flakeSat, 255);
            leds[new_pos + 1].setHSV(flakeColor, flakeSat, fadedBrightness);
        }

        FastLED.show();
        FastLED.delay(1000 / FRAMES_PER_SECOND);

    }

    fadeOut(0, NUM_LEDS);

}