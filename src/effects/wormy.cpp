//
// Created by bnf on 2021-02-04
//

// In support of Thomas Hudson's wall
// https://editor.p5js.org/hydronics2/present/YYg9urd-v
//
// A part of 2021 Portland Winter Light Festival
// https://www.pdxwlf.com/
//
// though hopefully this wall will bring great joy to many
// in our neighborhood for years to come
//
// Wormy
//
// A worm crawls down the line, occassionally stopping.
// Each time it stops it pauses, grows by one pixel, and
// changes direction.

#include "wormy.h"

#include "lib/shared.h"

const int SLOWESTPACE = 30;
const int BEGIN = 0;
const int END = NUM_LEDS;

// direction of travel
int direction;

// tail width
int tWidth;

int pace = random(SLOWESTPACE) + 1;

// head position
int hPos = BEGIN;
// the last point we paused at
int pPos = hPos;

void wormy(int duration) {
    FastLED.clear();
    resetStopwatch();

    tWidth = 1;
    direction = 1;
    hPos = BEGIN;
    pPos = hPos;

    while (stopwatch < duration * 1000) {
        updateTimers();

        // occassionally, stop for a moment
        int should_pause = random(500);
        if (should_pause < 5) {
            for (int i = 0; i < pace * should_pause; i++) {
                leds[hPos] = CHSV(random8(255), 255, 255);
                FastLED.show();
                FastLED.delay(1);
            }
            // when we stop, grow and change direction
            grow_worm_and_bounce();
        }

        // the head is always thinking
        leds[hPos] = CHSV(random8(255), 255, 255);

        // make sure the tail doesn't "whip around"
        // by calculating the width we shoud draw
        int w = tWidth;
        if (abs(hPos - pPos) < tWidth) {
            w = abs(hPos - pPos);
        }

        // the tail is always just following along
        for (int i = 1; i <= w; i++) {
            int tPixel = hPos - direction * i;  // a segment of the tail
            if (tPixel >= BEGIN && tPixel <= END) {
                leds[tPixel] = CHSV(random8(255), 255 / i, 255 / i);
            }
        }

        // turn off the pixel beyond the end of the tail
        int tailend = hPos - direction * tWidth;
        if (tailend >= BEGIN && tailend <= END) {
            leds[tailend] = CHSV(0, 0, 0);
        }

        FastLED.show();
        FastLED.delay(pace);  // walking pace

        // move the worm to the next pixel
        hPos = hPos + direction;

        // at the ends of the strand, grow the worm
        if (hPos >= END || hPos <= BEGIN) {
            grow_worm_and_bounce();
        }
    }
    fadeOut(0, NUM_LEDS);
}

void grow_worm_and_bounce() {
    direction = direction * -1;
    tWidth++;
    leds[SOUTH_END] = CHSV(0, 0, 0);
    pace = random(SLOWESTPACE) + 1;
    pPos = hPos;  // set pause position
}

// light up the east start and end, the corner and the south start and end
void orientation(int duration) {
    FastLED.clear();
    resetStopwatch();

    while (stopwatch < duration * 1000) {
        updateTimers();

        // random colored speckles that blink in and fade smoothly
        FastLED.delay(300);  // walking pace
        fadeOut(0, NUM_LEDS);
        leds[EAST_END] = CHSV(random8(255), 255, 255);
        leds[EAST_START] = CHSV(random8(255), 255, 255);
        FastLED.show();

        FastLED.delay(300);  // walking pace
        fadeOut(0, NUM_LEDS);
        leds[CENTER] = CHSV(random8(255), 255, 255);
        FastLED.show();

        FastLED.delay(300);  // walking pace
        fadeOut(0, NUM_LEDS);
        leds[SOUTH_START] = CHSV(random8(255), 255, 255);
        leds[SOUTH_END] = CHSV(random8(255), 255, 255);
        FastLED.show();
    }
    fadeOut(0, NUM_LEDS);
}
