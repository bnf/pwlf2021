//
// Created by James Moore on 1/19/21.
//

#include "shared.h"

void updateTimers() {
    ct = millis();
    dt = ct - lt;

    if (dt < 0) {
        dt = 0;
    }

    lt = ct;

    stopwatch += dt;

//    Serial.printf("CT: %i\tDT: %i\tSW: %i\n", ct, dt, stopwatch);
}

void resetStopwatch() {
    stopwatch = 0;
}

void fadeOut(int start, int end) {

    for (int f = 0; f < 12; f++) {
        for (int i = start; i <= end; i++) {
            leds[i].fadeToBlackBy(80);
        }
        FastLED.show();
        FastLED.delay(1000 / 150);
    }
    FastLED.clear();

    FastLED.delay(1000);
}

float float_rand( float min, float max ) {
    float scale = random() / (float) RAND_MAX; /* [0, 1.0] */
    return min + scale * ( max - min );      /* [min, max] */
}
