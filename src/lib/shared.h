//
// Created by James Moore on 1/19/21.
//

#ifndef PWLF2021_SHARED_H
#define PWLF2021_SHARED_H

#define FASTLED_ALLOW_INTERRUPTS 0
#include "FastLED.h"
#include "easing.h"
#include "trng.h"

#define _NUM_LEDS 120

extern const int EAST_START;
extern const int EAST_END;
extern const int CENTER;
extern const int SOUTH_START;
extern const int SOUTH_END;
extern const int NUM_LEDS;
extern const int DATA_PIN;
extern const int BRIGHTNESS;

extern int ct;
extern int lt;
extern int dt;
extern int stopwatch;

extern CRGB leds[];

void updateTimers();
void resetStopwatch();
void fadeOut(int start, int end);
float float_rand( float min, float max );

#endif //PWLF2021_SHARED_H
