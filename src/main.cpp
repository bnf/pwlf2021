#include <effects/fireworks.h>
#include <effects/ledman.h>
#include <effects/planets.h>
#include <effects/rgb.h>
#include <effects/small.h>
#include <effects/wormy.h>

#include "effects/countdown.h"
#include "effects/fire.h"
#include "effects/meteor.h"
#include "effects/pacifica.h"
#include "effects/pendulum.h"
#include "effects/snow.h"
#include "lib/shared.h"
const int EAST_START = 72;
const int EAST_END = 0;
const int CENTER = 73;
const int SOUTH_START = 74;

#ifndef TEENSYLC
const int DATA_PIN = 27;
const int SOUTH_END = 120;
#endif
#ifdef TEENSYLC
const int DATA_PIN = 17;  // teensylc 5v datapin
const int SOUTH_END = 119;
#endif

const int NUM_LEDS = _NUM_LEDS;
const int BRIGHTNESS = 255;
int ct = 0;
int lt = 0;
int dt = 0;
int stopwatch = 0;

#define COLOR_ORDER GRB  // Testing
//#define COLOR_ORDER RBG // Production

CRGB leds[NUM_LEDS];
uint8_t gHue = 0;

void setup() {
    Serial.begin(115200);
    //    while (!Serial); // wait for serial connection before starting

    FastLED.delay(1000);  // sanity delay
    FastLED.addLeds<WS2811, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
    FastLED.setBrightness(BRIGHTNESS);
    FastLED.clear();
    FastLED.show();

#ifndef TEENSYLC
    trng_init();
    random16_add_entropy(trng());
#endif

    ct = millis();
    lt = ct;
}

void loop() {
    countdown(15);
    planets(15);    // seconds
    meteorRain(5);  // loops
    juggle(15);     // seconds
    Fire(15);       // seconds
    pacifica(20);
    pendulum();
    confetti(15);  // seconds
    ledman();
    wormy(30);
    fireworks(20);  // seconds
}
